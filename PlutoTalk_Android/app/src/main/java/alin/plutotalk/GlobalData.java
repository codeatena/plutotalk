package alin.plutotalk;

import com.quickblox.users.model.QBUser;

import alin.plutotalk.model.User;

/**
 * Created by User on 6/21/2015.
 */
public class GlobalData {

    public static GlobalData instance;

    public QBUser curUser;

    public static GlobalData getInstance() {
        if (instance == null) {
            instance = new GlobalData();
        }
        return instance;
    }

    public GlobalData() {

    }
}
