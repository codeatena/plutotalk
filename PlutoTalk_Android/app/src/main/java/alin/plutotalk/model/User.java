package alin.plutotalk.model;

import android.graphics.Bitmap;
import android.media.Image;

import com.quickblox.users.model.QBUser;

import org.json.JSONObject;

import alin.plutotalk.utility.ImageUtility;

public class User extends QBUser {

    public int userNumber;

    public User(int userNumber, String fullName, String login, String password, int id) {
        super.fullName = fullName;
        super.login = login;
        super.password = password;
        super.id = id;
        this.userNumber = userNumber;
    }

    public static User getUser(QBUser qbUser) {
        User user = new User(-1, qbUser.getFullName(), qbUser.getLogin(), qbUser.getPassword(), qbUser.getId());
        return user;
    }

    public int getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(int userNumber) {
        this.userNumber = userNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (userNumber != user.userNumber) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + userNumber;
        return result;
    }

    public Bitmap getPhotoBitmap() {
        return getPhotoBitmapFromCustomData(getCustomData());
    }

    public String getPhotoString() {
        return getPhotoStringFromCusomData(getCustomData());
    }

    public static String getPhotoStringFromCusomData(String customData) {
        try {
            JSONObject jsonObject = new JSONObject(customData);
            String strBmp = jsonObject.getString("photo");
            return strBmp;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getPhotoBitmapFromCustomData(String cusomData) {
        return ImageUtility.StringToBitmap(getPhotoStringFromCusomData(cusomData));
    }

    public static String getCustomDataFromPhotoString(String strPhoto) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("photo", strPhoto);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public static String getCustomDataFromPhotoBitmap(Bitmap bmp) {
        return getCustomDataFromPhotoString(ImageUtility.BitmapToString(bmp));
    }
}
