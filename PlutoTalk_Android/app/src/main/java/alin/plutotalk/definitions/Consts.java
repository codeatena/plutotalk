package alin.plutotalk.definitions;

public class Consts {

    public static final String PROJECT_NUMBER = "895923018992";

    public static final int CALL_ACTIVITY_CLOSE = 1000;

    //CALL ACTIVITY CLOSE REASONS
    public static final int CALL_ACTIVITY_CLOSE_WIFI_DISABLED = 1001;
    public static final String WIFI_DISABLED = "wifi_disabled";
}
