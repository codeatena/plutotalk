package alin.plutotalk.utility;

import android.content.Context;
import android.content.SharedPreferences;

import alin.plutotalk.ApplicationSingleton;

/**
 * Created by User on 6/16/2015.
 */
public class PreferenceUtility {

    public final String PREF_NAME = "plutotalk_pref";

    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor prefEditor;

    public static PreferenceUtility instance = null;

    public static final String PREFERENCE_KEY_USER_STATUS = "user_status";
    public static final String PREFERENCE_KEY_USER_EMAIL = "user_email";
    public static final String PREFERENCE_KEY_USER_FULLNAME = "user_fullname";
    public static final String PREFERENCE_KEY_USER_PASSWORD = "user_password";
    public static final String PREFERENCE_KEY_USER_PHOTO = "user_photo";

    public static PreferenceUtility getInstance() {

        if (instance == null) {
            instance = new PreferenceUtility();
        }

        return instance;
    }

    public PreferenceUtility() {
        sharedPreferences = ApplicationSingleton.getInstance().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        prefEditor = sharedPreferences.edit();
    }

    public void saveUserInf(String email, String fullname, String password, String strPhoto) {
        prefEditor.putString(PREFERENCE_KEY_USER_STATUS, "login");
        prefEditor.putString(PREFERENCE_KEY_USER_EMAIL, email);
        prefEditor.putString(PREFERENCE_KEY_USER_FULLNAME, fullname);
        prefEditor.putString(PREFERENCE_KEY_USER_PASSWORD, password);
        prefEditor.putString(PREFERENCE_KEY_USER_PHOTO, strPhoto);
        prefEditor.commit();
    }

    public void logoutUser(String email, String fullname, String password, String strPhoto) {
        prefEditor.putString(PREFERENCE_KEY_USER_STATUS, "");
        prefEditor.commit();
    }
}
