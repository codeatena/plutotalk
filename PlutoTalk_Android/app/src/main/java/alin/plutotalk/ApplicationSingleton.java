package alin.plutotalk;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.quickblox.chat.model.QBDialog;
import com.quickblox.users.model.QBUser;

import java.util.HashMap;
import java.util.Map;

public class ApplicationSingleton extends Application {

    private static ApplicationSingleton instance;

    public final static String OPPONENTS = "opponents";
    public static final String CONFERENCE_TYPE = "conference_type";

    private QBUser currentUser;

    private Map<Integer, QBUser> dialogsUsers = new HashMap<Integer, QBUser>();

    @Override
    public void onCreate() {
        super.onCreate();
        initApplication();
    }

    public QBUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(QBUser currentUser) {
        this.currentUser = currentUser;
    }

    public Map<Integer, QBUser> getDialogsUsers() {
        return dialogsUsers;
    }


    public Integer getOpponentIDForPrivateDialog(QBDialog dialog) {
        Integer opponentID = -1;
        for (Integer userID : dialog.getOccupants()) {
            if (!userID.equals(getCurrentUser().getId())) {
                opponentID = userID;
                break;
            }
        }
        return opponentID;
    }

    public static ApplicationSingleton getInstance() {
        return instance;
    }

    private void initApplication() {
        instance = this;
        
//        PreferenceUtility.sharedPreferences = getSharedPreferences(PreferenceUtility.PREF_NAME, MODE_PRIVATE);
//        PreferenceUtility.prefEditor = PreferenceUtility.sharedPreferences.edit();

//        GlobalData.dbHelper = new DatabaseHelper(this);
    }

    public int getAppVersion() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}