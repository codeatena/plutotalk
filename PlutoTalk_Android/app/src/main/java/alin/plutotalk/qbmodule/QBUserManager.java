package alin.plutotalk.qbmodule;

import android.graphics.Bitmap;

import com.quickblox.users.model.QBUser;

import org.json.JSONObject;

import alin.plutotalk.utility.ImageUtility;

/**
 * Created by User on 6/20/2015.
 */
public class QBUserManager {

    public static QBUserManager instance;

    public static QBUserManager getInstance() {

        if (instance == null) {
            instance = new QBUserManager();
        }

        return instance;
    }

    public static Bitmap getPhotoBitmapFromCustomData(QBUser qbUser) {
        try {
            JSONObject jsonObject = new JSONObject(qbUser.getCustomData());
            String strBmp = jsonObject.getString("photo");
            return ImageUtility.StringToBitmap(strBmp);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getPhotoStringFromCustomData(QBUser qbUser) {
        try {
            JSONObject jsonObject = new JSONObject(qbUser.getCustomData());
            String strBmp = jsonObject.getString("photo");
            return strBmp;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
