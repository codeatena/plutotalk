package alin.plutotalk.listView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.quickblox.users.model.QBUser;

import org.json.JSONObject;

import java.util.ArrayList;

import alin.plutotalk.R;
import alin.plutotalk.qbmodule.QBUserManager;
import alin.plutotalk.utility.ImageUtility;

public class MyListAdapter extends ArrayAdapter<MyListItem> {

    public MyListAdapter(Context context, ArrayList<MyListItem> listItems) {
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(getContext());

        MyListItem item = getItem(position);

        if (item instanceof MyListItem) {
            view = inflater.inflate(item.layoutID, parent, false);
            Object data = item.data;

            if (data instanceof QBUser) {
                ImageView imgPhoto = (ImageView) view.findViewById(R.id.photo_imageView);
                TextView txtUserName = (TextView) view.findViewById(R.id.username_textView);

                QBUser qbUser = (QBUser) data;

                imgPhoto.setImageBitmap(QBUserManager.getPhotoBitmapFromCustomData(qbUser));
                txtUserName.setText(qbUser.getFullName());
            }
        }

        return view;
    }
}