package alin.plutotalk.listView.fragment;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

import alin.plutotalk.GlobalData;
import alin.plutotalk.R;
import alin.plutotalk.listView.MyListAdapter;
import alin.plutotalk.listView.MyListItem;
import alin.plutotalk.model.User;
import alin.plutotalk.utility.DialogUtility;
import alin.plutotalk.utility.PreferenceUtility;

/**
 * Created by User on 6/20/2015.
 */
public class UsersListFragment extends ListFragment {

    private ArrayList<MyListItem> mItems;
    int curPage = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mItems = new ArrayList<MyListItem>();
//        setListAdapter(new MyListAdapter(getActivity(), mItems));
        readAllUser();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        MyListItem myListItem = mItems.get(position);
        QBUser qbUser = (QBUser) myListItem.data;
    }

    private void readAllUser() {
        QBUsers.getUsers(null, new QBEntityCallbackImpl<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
                mItems.clear();

                for (QBUser qbUser: qbUsers) {
                    if (qbUser.getEmail().equals(GlobalData.getInstance().curUser.getEmail())) {
                        String curUserEmail = PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_EMAIL, "");
                        String curPassword = PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PASSWORD, "");

                        qbUser.setLogin(curUserEmail);
                        qbUser.setPassword(curPassword);
                        GlobalData.getInstance().curUser = qbUser;

                        continue;
                    }
                    MyListItem myListItem = new MyListItem(R.layout.row_user, qbUser);
                    mItems.add(myListItem);
                }

                setListAdapter(new MyListAdapter(getActivity(), mItems));
            }

            @Override
            public void onError(List<String> errors) {
                DialogUtility.show(getActivity(), errors.get(0));
            }
        });
    }
}