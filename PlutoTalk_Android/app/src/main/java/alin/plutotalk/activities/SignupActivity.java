package alin.plutotalk.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBSignaling;
import com.quickblox.chat.QBWebRTCSignaling;
import com.quickblox.chat.listeners.QBVideoChatSignalingManagerListener;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;

import org.json.JSONObject;

import java.util.List;
import java.util.Properties;

import alin.plutotalk.GlobalData;
import alin.plutotalk.R;
import alin.plutotalk.model.User;
import alin.plutotalk.utility.CameraUtility;
import alin.plutotalk.utility.DialogUtility;
import alin.plutotalk.utility.ImageUtility;
import alin.plutotalk.utility.PreferenceUtility;

public class SignupActivity extends Activity {

    EditText edtFullName;
    EditText edtEmail;
    EditText edtPassword;
    EditText edtConfirm;

    ImageView imvPhoto;
    Bitmap bmpPhoto;
    Button btnSingup;

    private Uri fileUri;
    int nGetPicture;

    final int NOTHING_MODE = 0;
    final int TAKE_PICTURE_MODE = 1;
    final int SELECT_PICTURE_MODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        edtFullName = (EditText) findViewById(R.id.fullname_editText);
        edtEmail = (EditText) findViewById(R.id.email_editText);
        edtPassword = (EditText) findViewById(R.id.password_editText);
        edtConfirm = (EditText) findViewById(R.id.confirm_editText);

        imvPhoto = (ImageView) findViewById(R.id.photo_imageView);
        btnSingup = (Button) findViewById(R.id.signup_button);
    }

    private void initValue() {
        nGetPicture = NOTHING_MODE;
        bmpPhoto = null;
    }

    private void initEvent() {
        imvPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePictureDialog();
            }
        });
        btnSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signup();
            }
        });
    }

    private void signup() {

        final String fullName = edtFullName.getText().toString();
        final String email = edtEmail.getText().toString();
        final String password = edtPassword.getText().toString();
        final String confirm = edtConfirm.getText().toString();

        if (fullName.equals("")) {
            DialogUtility.show(this, "Please input your full name.");
            return;
        }
        if (email.equals("")) {
            DialogUtility.show(this, "Please input your email.");
            return;
        }
        if (password.equals("")) {
            DialogUtility.show(this, "Please input your password.");
            return;
        }
        if (password.length() < 8) {
            DialogUtility.show(this, "You must enter 8 charaters at least for password.");
            return;
        }
        if (!confirm.equals(password)) {
            DialogUtility.show(this, "Passwords are not mathced.");
            return;
        }
        if (bmpPhoto == null) {
            DialogUtility.show(this, "Please take your photo");
            return;
        }

        QBUser qbUser = new QBUser();
        qbUser.setLogin(email);
        qbUser.setFullName(fullName);
        qbUser.setPassword(password);
        qbUser.setEmail(email);

        final String strPhoto = ImageUtility.BitmapToString(bmpPhoto);
        qbUser.setCustomData(User.getCustomDataFromPhotoString(strPhoto));

        final ProgressDialog progDlg = DialogUtility.getProgressDialog(this);
        progDlg.show();

        QBUsers.signUpSignInTask(qbUser, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
//                progDlg.dismiss();
                PreferenceUtility.getInstance().saveUserInf(email, fullName, password, strPhoto);

                qbUser.setLogin(email);
                qbUser.setPassword(password);
                GlobalData.getInstance().curUser = qbUser;

                // INIT CHAT SERVICE
                if (!QBChatService.isInitialized()) {
                    Log.e("chat", "is not null");
                    QBChatService.init(SignupActivity.this);
                }

                // LOG IN CHAT SERVICE
                if (QBChatService.isInitialized()) {
                    Log.e("chatservice result", "is not null");
                    QBChatService.getInstance().login(qbUser, new QBEntityCallbackImpl<QBUser>() {
                        @Override
                        public void onSuccess() {
                            // success
                            progDlg.dismiss();
                            Log.e("chatservice result", "success");
                            QBChatService.getInstance().getVideoChatWebRTCSignalingManager().addSignalingManagerListener(new QBVideoChatSignalingManagerListener() {
                                @Override
                                public void signalingCreated(QBSignaling qbSignaling, boolean createdLocally) {
                                    if (!createdLocally) {
                                        QBRTCClient.getInstance().addSignaling((QBWebRTCSignaling) qbSignaling);
                                    }
                                }
                            });
                        }

                        @Override
                        public void onError(List<String> errors) {
                            //error
                            progDlg.dismiss();
                            DialogUtility.showLong(SignupActivity.this, errors.get(0));
                        }
                    });
                }
                progDlg.dismiss();
                Intent intent = new Intent(SignupActivity.this, CallActivity.class);
                intent.putExtra("login", email);
                startActivity(intent);
                finish();
            }

            @Override
            public void onError(List<String> errors) {
                progDlg.dismiss();
                DialogUtility.show(SignupActivity.this, errors.get(0));
            }
        });
    }

    private void takePictureDialog() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

            private static final int TAKE_PICTURE = 0;
            private static final int SELECT_PICTURE = 1;
            private static final int CANCEL = 2;

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case TAKE_PICTURE:
                        Log.e("TAKE_PICTURE", "click");
                        takePhoto();
                        break;
                    case SELECT_PICTURE:
                        Log.e("SELECT_PICTURE", "click");
                        selectPhoto();
                        break;
                    case CANCEL:
                        dialog.dismiss();
                        break;
                }
            }
        };

        new AlertDialog.Builder(SignupActivity.this)
                .setItems(R.array.dialog_get_picture, listener)
                .setCancelable(true)
                .show();
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = CameraUtility.getOutputMediaFileUri(CameraUtility.MEDIA_TYPE_IMAGE);

        try {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } catch (Exception e) {
            e.printStackTrace();
        }

        startActivityForResult(intent, CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public void selectPhoto() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, CameraUtility.SELECT_PICTURE_REQUEST_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturedIntent) {
        if (requestCode == CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                nGetPicture = TAKE_PICTURE_MODE;
                previewImage();
            }
        }
        if (requestCode == CameraUtility.SELECT_PICTURE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                nGetPicture = SELECT_PICTURE_MODE;
                fileUri = imageReturedIntent.getData();
                fileUri = ImageUtility.getRealUriFromContentUri(fileUri);
                previewImage();
            }
        }
    }

    private void previewImage() {
        bmpPhoto = ImageUtility.getBitmapFromFileUri(fileUri, 30, 30);
        imvPhoto.setImageBitmap(bmpPhoto);
        Log.e("Image File path", fileUri.getPath());
    }

    private String getUserCustomDataString() {
        JSONObject jsonObject = new JSONObject();

        try {
            String strBitmap = ImageUtility.BitmapToString(bmpPhoto);
            jsonObject.put("photo", strBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("photo string: ", jsonObject.toString());
        return jsonObject.toString();
    }
}