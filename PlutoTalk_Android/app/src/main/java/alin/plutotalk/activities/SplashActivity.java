package alin.plutotalk.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmReceiver;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBSignaling;
import com.quickblox.chat.QBWebRTCSignaling;
import com.quickblox.chat.listeners.QBVideoChatSignalingManagerListener;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;

import org.json.JSONObject;

import java.util.List;

import alin.plutotalk.ApplicationSingleton;
import alin.plutotalk.GlobalData;
import alin.plutotalk.R;
import alin.plutotalk.model.User;
import alin.plutotalk.qbmodule.QBConst;
import alin.plutotalk.utility.DialogUtility;
import alin.plutotalk.utility.ImageUtility;
import alin.plutotalk.utility.PreferenceUtility;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
    }

    private void initValue() {
        QBSettings.getInstance().fastConfigInit(QBConst.APP_ID, QBConst.AUTH_KEY, QBConst.AUTH_SECRET);
    }

    private void initEvent() {
        autoLogin();
    }

    private void createSession() {
        QBAuth.createSession(new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession qbSession, Bundle bundle) {

                Log.e("auth result", "sucess");
                Log.e("qbtoken", qbSession.getToken());

                if (!QBChatService.isInitialized()) {
                    QBChatService.init(ApplicationSingleton.getInstance());
                }

                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }

            @Override
            public void onError(List<String> errors) {
                // print errors that came from server
                DialogUtility.showLong(SplashActivity.this, errors.get(0));
            }
        });
    }

    private void autoLogin() {
        String curUserStatus = PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_STATUS, "");
        if (!curUserStatus.equals("login")) {
            createSession();
        } else {

            final String curUserEmail = PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_EMAIL, "");
            final String curPassword = PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PASSWORD, "");
            final String curFullName = PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_FULLNAME, "");
            final String curPhoto = PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PHOTO, "");
//            new QBMyUserService(this, false).login(curUserName, curPassword);

            Log.e("current user", curUserEmail + ", " + curPassword);

            final QBUser user = new QBUser(curUserEmail, curPassword);

            QBAuth.createSession(curUserEmail, curPassword, new QBEntityCallbackImpl<QBSession>() {
                @Override
                public void onSuccess(QBSession session, Bundle bundle) {

                    user.setId(session.getUserId());
                    user.setEmail(curUserEmail);
                    user.setPassword(curPassword);
                    user.setLogin(curUserEmail);
                    user.setFullName(curFullName);
                    user.setCustomData(User.getCustomDataFromPhotoString(curPhoto));

                    GlobalData.getInstance().curUser = user;
                    // INIT CHAT SERVICE
                    if (!QBChatService.isInitialized()) {
                        Log.e("chat", "is not null");
                        QBChatService.init(SplashActivity.this);
                    }



                    // LOG IN CHAT SERVICE
                    if (QBChatService.isInitialized()) {
                        Log.e("chatservice result", "is not null");

                        QBChatService.getInstance().login(user, new QBEntityCallbackImpl<QBUser>() {
                            @Override
                            public void onSuccess() {
                                // success
                                Log.e("chatservice result", "success");
                                QBChatService.getInstance().getVideoChatWebRTCSignalingManager().addSignalingManagerListener(new QBVideoChatSignalingManagerListener() {
                                    @Override
                                    public void signalingCreated(QBSignaling qbSignaling, boolean createdLocally) {
                                        if (!createdLocally) {
                                            QBRTCClient.getInstance().addSignaling((QBWebRTCSignaling) qbSignaling);
                                        }
                                    }
                                });
                            }

                            @Override
                            public void onError(List errors) {
                                DialogUtility.showLong(SplashActivity.this, errors.get(0).toString());
                            }
                        });
                    }

                    Intent intent = new Intent(SplashActivity.this, CallActivity.class);
                    intent.putExtra("login", curUserEmail);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onError(List<String> errors) {
                    //error
//                    DialogUtility.showLong(SplashActivity.this, errors.get(0));
                }
            });
        }
    }
}
