package alin.plutotalk.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.Image;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import alin.plutotalk.GlobalData;
import alin.plutotalk.R;
import alin.plutotalk.model.DataHolder;
import alin.plutotalk.model.User;


/**
 * Created by tereha on 26.01.15.
 */
public class BaseLogginedUserActivity extends Activity {

    private static final String VERSION_NUMBER = "0.9.4.18062015";
    private static final String APP_VERSION = "App version";
    static android.app.ActionBar mActionBar;
    private Chronometer timerABWithTimer;
    private boolean isStarted = false;

    public void initActionBar() {

        mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);

        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.actionbar_view, null);

        ImageView imgPhoto = (ImageView) mCustomView.findViewById(R.id.photo_imageView);
        TextView loginAsAB = (TextView) mCustomView.findViewById(R.id.loginAsAB);
        TextView userNameAB = (TextView) mCustomView.findViewById(R.id.userNameAB);

        imgPhoto.setImageBitmap(User.getPhotoBitmapFromCustomData(GlobalData.getInstance().curUser.getCustomData()));
        loginAsAB.setText(R.string.logged_in_as);
        userNameAB.setText(" " + GlobalData.getInstance().curUser.getFullName());

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    public void initActionBarWithTimer() {
        mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);

        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.actionbar_with_timer, null);

        timerABWithTimer = (Chronometer) mCustomView.findViewById(R.id.timerABWithTimer);

        TextView loginAsABWithTimer = (TextView) mCustomView.findViewById(R.id.loginAsABWithTimer);
        loginAsABWithTimer.setText(R.string.logged_in_as);

        TextView userNameAB = (TextView) mCustomView.findViewById(R.id.userNameABWithTimer);
        userNameAB.setText(GlobalData.getInstance().curUser.getFullName());

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    public void startTimer() {
        if (!isStarted) {
            timerABWithTimer.setBase(SystemClock.elapsedRealtime());
            timerABWithTimer.start();
            isStarted = true;
        }
    }

    public void stopTimer(){
        if (timerABWithTimer != null){
            timerABWithTimer.stop();
            isStarted = false;
        }
    }
}




