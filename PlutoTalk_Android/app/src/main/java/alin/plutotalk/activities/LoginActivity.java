package alin.plutotalk.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBSignaling;
import com.quickblox.chat.QBWebRTCSignaling;
import com.quickblox.chat.listeners.QBVideoChatSignalingManagerListener;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;

import org.json.JSONObject;

import java.util.List;

import alin.plutotalk.GlobalData;
import alin.plutotalk.R;
import alin.plutotalk.model.User;
import alin.plutotalk.qbmodule.QBUserManager;
import alin.plutotalk.utility.DialogUtility;
import alin.plutotalk.utility.PreferenceUtility;


public class LoginActivity extends Activity {

    EditText edtEmail;
    EditText edtPassword;
    Button btnLogin;
    Button btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        edtEmail = (EditText) findViewById(R.id.email_editText);
        edtPassword = (EditText) findViewById(R.id.password_editText);
        btnLogin = (Button) findViewById(R.id.login_button);
        btnSignup = (Button) findViewById(R.id.signup_button);
    }

    private void initValue() {
//        edtEmail.setText("alinhila@yahoo.com");
//        edtPassword.setText("qwertyui");
    }

    private void initEvent() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String email = edtEmail.getText().toString();
                final String password = edtPassword.getText().toString();

                final QBUser user = new QBUser(email, password);
                user.setEmail(email);

                final ProgressDialog progDlg = DialogUtility.getProgressDialog(LoginActivity.this);
                progDlg.show();

                QBUsers.signIn(user, new QBEntityCallbackImpl<QBUser>() {
                    @Override
                    public void onSuccess(QBUser qbUser, Bundle params) {

                        Log.e("login result", "success");
                        String customData = user.getCustomData();
                        String strPhoto = QBUserManager.getPhotoStringFromCustomData(qbUser);
                        PreferenceUtility.getInstance().saveUserInf(email, user.getFullName(), password, strPhoto);

                        qbUser.setLogin(email);
                        qbUser.setPassword(password);
                        GlobalData.getInstance().curUser = qbUser;

                        QBChatService chatService = null;
                        // INIT CHAT SERVICE
                        if (!QBChatService.isInitialized()) {
                            Log.e("chat", "is not null");
                            QBChatService.init(LoginActivity.this);
                        }

                        // LOG IN CHAT SERVICE
                        if (QBChatService.isInitialized()) {
                            Log.e("chatservice result", "is not null");

                            chatService.login(user, new QBEntityCallbackImpl<QBUser>() {
                                @Override
                                public void onSuccess() {
                                    // success
                                    Log.e("chatservice result", "success");
                                    QBChatService.getInstance().getVideoChatWebRTCSignalingManager().addSignalingManagerListener(new QBVideoChatSignalingManagerListener() {
                                        @Override
                                        public void signalingCreated(QBSignaling qbSignaling, boolean createdLocally) {
                                            if (!createdLocally) {
                                                QBRTCClient.getInstance().addSignaling((QBWebRTCSignaling) qbSignaling);
                                            }
                                        }
                                    });
                                }

                                @Override
                                public void onError(List<String> errors) {
                                    //error
                                    progDlg.dismiss();
                                    Log.e("chatservice result", "failed");
                                    DialogUtility.showLong(LoginActivity.this, errors.get(0));
                                }
                            });
                        }
                        progDlg.dismiss();
                        Intent intent = new Intent(LoginActivity.this, CallActivity.class);
                        intent.putExtra("login", email);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onError(List<String> errors) {
                        progDlg.dismiss();
                        DialogUtility.show(LoginActivity.this, errors.get(0));
                    }
                });
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view){
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
                finish();
            }
        });
    }
}